var projectKey = '__UP_project_key__';
function initVersions() {
	if(projectKey) {
		$.ajax({
			type: 'GET',
			url: '/rest/api/2/project/' + projectKey + '/versions',
			success: function(data) {
				console.log(data);
			},
			cache: false
		});
	}
}
